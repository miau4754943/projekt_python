# game settings

"""screen"""
WIDTH = 600
HEIGHT = 780
FPS = 60
title = "Wordle! - Marta Piotrowska"

"""tiles"""
TILESIZE = 80
GAPSIZE = 10

"""margin - it makes that board is always in the centre of the screen"""
MARGIN_X = int((WIDTH - (5 * (TILESIZE + GAPSIZE))) / 2)
MARGIN_Y = int((HEIGHT - (6 * (TILESIZE + GAPSIZE))) / 2)

